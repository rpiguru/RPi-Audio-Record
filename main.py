import datetime
import queue
import time
import copy
import sys
import sounddevice as sd
import threading
import RPi.GPIO as GPIO
from settings import DOUBLE_CLICK_PERIOD, PIN_BUTTON, AUDIO_CHUNK, KEEP_PERIOD, PLAY_PERIOD


GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_BUTTON, GPIO.IN)

sd.default.blocksize = AUDIO_CHUNK
sd.default.never_drop_input = False

device_info = sd.query_devices(0, 'input')
samplerate = int(device_info['default_samplerate'])
channels = 1

record_frame_count = int(samplerate / AUDIO_CHUNK * KEEP_PERIOD * 60)
play_frame_count = int(samplerate / AUDIO_CHUNK * PLAY_PERIOD)


class RPiAudioRecord:

    is_playing = threading.Event()
    lock = threading.RLock()
    last_pressed_time = 0
    buf = []
    play_buf = []
    play_index = 0
    q = queue.Queue()

    def __init__(self):
        self.is_playing.clear()

    def run(self):
        with sd.Stream(device=(0, 0), samplerate=samplerate, channels=channels, callback=self.callback):
            while True:
                self.buf.append(self.q.get())
                if len(self.buf) > record_frame_count:
                    self.buf = self.buf[-record_frame_count:]

    def callback(self, indata, outdata, frames, _time, status):
        """This is called (from a separate thread) for each audio block."""
        if status:
            print(status, file=sys.stderr)
        if self.is_playing.isSet():
            if self.play_index < len(self.play_buf):
                outdata[:] = copy.deepcopy(self.play_buf[self.play_index])
                self.play_index += 1
            else:
                print('{}: Finished playback.'.format(datetime.datetime.now()))
                self.is_playing.clear()
                self.play_index = 0
                outdata[:] = indata
        else:
            outdata[:] = indata
        self.q.put(indata.copy())

    def on_button_pressed(self, channel):
        if self.is_playing.isSet():
            print('{}: Button is pressed, but the audio is already playing now...'.format(datetime.datetime.now()))
            return

        if time.time() - self.last_pressed_time < DOUBLE_CLICK_PERIOD / 1000.:
            print('{}: Button is double-clicked!'.format(datetime.datetime.now()))
        else:
            print('{}: Button is pressed!'.format(datetime.datetime.now()))
            length = play_frame_count if len(self.buf) > play_frame_count else len(self.buf)
            self.play_buf = copy.deepcopy(self.buf[-length:])

        self.is_playing.set()
        self.last_pressed_time = time.time()


if __name__ == '__main__':

    w = RPiAudioRecord()

    GPIO.add_event_detect(PIN_BUTTON, GPIO.FALLING, callback=w.on_button_pressed, bouncetime=200)

    w.run()
