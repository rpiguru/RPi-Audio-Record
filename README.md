# Components

- Raspberry Pi Zero
- Fe-Pi Audio Z V2 Sound Card

# Scenario

The Pi will be hock up to a radio, and sometimes the people on there talk too fast
or you are distracted with another task to get the info the first time.

So then you can click the button an get the last 10 seconds again. 

But in the event of the replay, the recording of the "live" stream needs to continue. 

When you double-click the button it should replay the message again and one-click plays the latest 10 seconds etc..

So the audio needs to record constantly but only keep the audio for 3-minute time span and then you can overwrite it so the memory won't run out.

# Requirements
- Record constantly from Line / Mic input for 3 min in a loop. 
- When a button is pressed (GPIO) playback the last 10 Seconds from the loop at the "line out" output. But still, keep recording.
- When the button is pressed again 2 in a short time (double click) play the same 10 seconds again. If pressed only once play the latest 10 second from the loop.

# Connection.
    
- Connect a *pull-up**push button to **GPIO21** of the RPi.

    See [here](http://raspi.tv/2013/rpi-gpio-basics-6-using-inputs-and-outputs-together-with-rpi-gpio-pull-ups-and-pull-downs) 
    if you are not clear about the *pull-up* button.

# Installation
    
- Clone this repo.
    
        cd ~
        git clone https://gitlab.com/rpiguru/RPi-Audio-Record

- Install now.
        
        cd ~/RPi-Audio-Record
        bash setup.sh

# Configure **alsamixer**

![alsamixer](alsamixer_config.jpg)

And reboot!
