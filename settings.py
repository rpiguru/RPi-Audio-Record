"""
General Configurations
"""

# Use GPIO21 for the button.
PIN_BUTTON = 21

# What is the maximum threshold of double-clicking in milliseconds?
DOUBLE_CLICK_PERIOD = 500

# How long do we keep the audio in minutes?
KEEP_PERIOD = 3

# How long do we play the audio again in seconds?
PLAY_PERIOD = 10

AUDIO_RATE = 44100
AUDIO_CHANNELS = 2
AUDIO_CHUNK = 1024
