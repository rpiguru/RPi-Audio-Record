#!/usr/bin/env bash

sudo apt-get update

sudo apt-get install python3 libpython3-dev python3-pip

sudo pip3 install -U pip

sudo apt-get install -y portaudio19-dev
sudo pip3 install numpy cffi
sudo pip3 install pyaudio sounddevice soundfile

echo "Adding Device Tree Overlays of Fe-Pi Audio Board..."
echo "dtoverlay=fe-pi-audio" | sudo tee -a /boot/config.txt
echo "dtoverlay=i2s-mmap" | sudo tee -a /boot/config.txt

echo "Disable the on-board RPi Sound device"
sudo sed -i -- "s/dtparam=audio=on/dtparam=audio=off/" /boot/config.txt

# Auto-start
sudo sed -i -- "s/^exit 0/(cd \/home\/pi\/RPi-Audio-Record \&\& python3 main.py)\&\\nexit 0/g" /etc/rc.local


sudo reboot
